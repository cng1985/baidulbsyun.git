package com.nbsaas.lbs.qq.v1.domain.response;

import java.io.Serializable;

public class AreaInfo implements Serializable {
  
  private String adcode;
  
  private String province;
  private String city;
  private String district;
  
  public String getAdcode() {
    return adcode;
  }
  
  public void setAdcode(String adcode) {
    this.adcode = adcode;
  }
  
  public String getProvince() {
    return province;
  }
  
  public void setProvince(String province) {
    this.province = province;
  }
  
  public String getCity() {
    return city;
  }
  
  public void setCity(String city) {
    this.city = city;
  }
  
  public String getDistrict() {
    return district;
  }
  
  public void setDistrict(String district) {
    this.district = district;
  }
}
