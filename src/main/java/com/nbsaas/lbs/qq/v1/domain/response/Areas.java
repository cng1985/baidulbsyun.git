package com.nbsaas.lbs.qq.v1.domain.response;

import java.io.Serializable;
import java.util.List;

public class Areas implements Serializable{
  
  private List<List<AreaItem>> result;
  
  private Integer status;
  
  private String message;
  
  private String data_version;
  
  public List<List<AreaItem>> getResult() {
    return result;
  }
  
  public void setResult(List<List<AreaItem>> result) {
    this.result = result;
  }
  
  public Integer getStatus() {
    return status;
  }
  
  public void setStatus(Integer status) {
    this.status = status;
  }
  
  public String getMessage() {
    return message;
  }
  
  public void setMessage(String message) {
    this.message = message;
  }
  
  public String getData_version() {
    return data_version;
  }
  
  public void setData_version(String data_version) {
    this.data_version = data_version;
  }
  
  @Override
  public String toString() {
    return "Areas{" +
        "result=" + result +
        ", status=" + status +
        ", message='" + message + '\'' +
        ", data_version='" + data_version + '\'' +
        '}';
  }
}
