package com.nbsaas.lbs.qq.v1.domain.response;

import java.io.Serializable;

public class Location implements Serializable {
  
  private Float lat;
  
  private Float lng;
  
  public Float getLat() {
    return lat;
  }
  
  public void setLat(Float lat) {
    this.lat = lat;
  }
  
  public Float getLng() {
    return lng;
  }
  
  public void setLng(Float lng) {
    this.lng = lng;
  }
  
  @Override
  public String toString() {
    return "Location{" +
        "lat=" + lat +
        ", lng=" + lng +
        '}';
  }
}
