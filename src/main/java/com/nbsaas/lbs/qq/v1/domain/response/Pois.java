package com.nbsaas.lbs.qq.v1.domain.response;

import java.util.List;

public class Pois {
  
  private List<Poi> data;
  
  private Integer status;
  
  private String message;
  
  private Integer count;
  
  public List<Poi> getData() {
    return data;
  }
  
  public void setData(List<Poi> data) {
    this.data = data;
  }
  
  public Integer getStatus() {
    return status;
  }
  
  public void setStatus(Integer status) {
    this.status = status;
  }
  
  public String getMessage() {
    return message;
  }
  
  public void setMessage(String message) {
    this.message = message;
  }
  
  public Integer getCount() {
    return count;
  }
  
  public void setCount(Integer count) {
    this.count = count;
  }
  
  @Override
  public String toString() {
    return "Pois{" +
        "data=" + data +
        ", status=" + status +
        ", message='" + message + '\'' +
        ", count=" + count +
        '}';
  }
}
