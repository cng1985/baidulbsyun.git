package com.nbsaas.lbs.qq.district.domain.simple;

import lombok.Data;

@Data
public class DistrictSimple {

    private String id;

    private String name;

    private String fullname;


    private Location location;

}
