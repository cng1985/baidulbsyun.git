package com.nbsaas.lbs.qq.district.app;

import com.google.gson.Gson;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.lbs.qq.district.domain.simple.DistrictSimple;
import com.nbsaas.lbs.qq.district.resource.DistrictResource;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RootApp {
    public static void main(String[] args) {

        DistrictResource districtResource = new DistrictResource();
        ListResponse<List<DistrictSimple>> data = districtResource.list();
        for (List<DistrictSimple> datum : data.getData()) {
            System.out.println(datum.size());
        }
    }
}
