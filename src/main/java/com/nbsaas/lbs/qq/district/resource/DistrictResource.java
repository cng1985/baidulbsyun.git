package com.nbsaas.lbs.qq.district.resource;

import com.google.gson.Gson;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.http.Connection;
import com.nbsaas.http.HttpConnection;
import com.nbsaas.lbs.qq.district.api.DistrictApi;
import com.nbsaas.lbs.qq.district.domain.simple.DistrictResult;
import com.nbsaas.lbs.qq.district.domain.simple.DistrictSimple;

import java.io.IOException;
import java.util.List;

public class DistrictResource implements DistrictApi {
    @Override
    public ListResponse<DistrictSimple> root() {
        ListResponse<DistrictSimple> result=new ListResponse<>();
        String sign=Md5Utils.getMD5Hash("/ws/district/v1/list?key=3GWBZ-7THRC-DZI2F-ABUQ7-CILMV-ACFB7kK27ubm3sYd79rofB9cs9PMHFqxcOdRy");

        Connection connection = HttpConnection.connect("https://apis.map.qq.com/ws/district/v1/list");
        connection.data("key", "3GWBZ-7THRC-DZI2F-ABUQ7-CILMV-ACFB7");
        connection.data("sig", sign);

        connection.method(Connection.Method.GET);
        String body = null;
        try {
            body = connection.execute().body();
            DistrictResult districtResult=  new Gson().fromJson(body, DistrictResult.class);
            System.out.println(districtResult.getResult().size());
            result.setData(districtResult.getResult().get(0));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public ListResponse<List<DistrictSimple>> list() {
        ListResponse<List<DistrictSimple>> result=new ListResponse<>();
        String sign=Md5Utils.getMD5Hash("/ws/district/v1/list?key=3GWBZ-7THRC-DZI2F-ABUQ7-CILMV-ACFB7kK27ubm3sYd79rofB9cs9PMHFqxcOdRy");

        Connection connection = HttpConnection.connect("https://apis.map.qq.com/ws/district/v1/list");
        connection.data("key", "3GWBZ-7THRC-DZI2F-ABUQ7-CILMV-ACFB7");
        connection.data("sig", sign);

        connection.method(Connection.Method.GET);
        String body = null;
        try {
            body = connection.execute().body();
            DistrictResult districtResult=  new Gson().fromJson(body, DistrictResult.class);
            System.out.println(districtResult.getResult().size());
            result.setData(districtResult.getResult());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public ListResponse<DistrictSimple> children(String code) {
        ListResponse<DistrictSimple> result=new ListResponse<>();
        String sign=Md5Utils.getMD5Hash(String.format("/ws/district/v1/getchildren?id=%s&key=3GWBZ-7THRC-DZI2F-ABUQ7-CILMV-ACFB7kK27ubm3sYd79rofB9cs9PMHFqxcOdRy",code));

        Connection connection = HttpConnection.connect("https://apis.map.qq.com/ws/district/v1/getchildren");
        connection.data("key", "3GWBZ-7THRC-DZI2F-ABUQ7-CILMV-ACFB7");
        connection.data("id", code);
        connection.data("sig", sign);

        connection.method(Connection.Method.GET);
        String body = null;
        try {
            body = connection.execute().body();
            DistrictResult districtResult=  new Gson().fromJson(body, DistrictResult.class);
            System.out.println(districtResult.getResult().size());
            result.setData(districtResult.getResult().get(0));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

}
