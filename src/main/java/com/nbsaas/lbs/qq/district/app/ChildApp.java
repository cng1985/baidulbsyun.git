package com.nbsaas.lbs.qq.district.app;

import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.lbs.qq.district.domain.simple.DistrictSimple;
import com.nbsaas.lbs.qq.district.resource.DistrictResource;

public class ChildApp {
    public static void main(String[] args) {

        DistrictResource districtResource=new DistrictResource();
        ListResponse<DistrictSimple> data = districtResource.children("811100");
        for (DistrictSimple datum : data.getData()) {
            System.out.println(datum);
        }
    }
}
