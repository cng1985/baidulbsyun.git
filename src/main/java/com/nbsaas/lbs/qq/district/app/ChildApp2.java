package com.nbsaas.lbs.qq.district.app;

import com.google.gson.Gson;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.lbs.qq.district.domain.simple.DistrictSimple;
import com.nbsaas.lbs.qq.district.resource.DistrictResource;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChildApp2 {
    public static void main(String[] args) {


        List<String> codes=new ArrayList<>();
        codes.add("520000");
        codes.add("650000");

        for (String code : codes) {
            updateChildren(code);

        }


    }

    @NotNull
    private static List<String> getStrings() {
        List<String> codes=new ArrayList<>();
        codes.add("140000");
        codes.add("150000");
        codes.add("210000");
        codes.add("220000");
        codes.add("230000");
        codes.add("320000");
        codes.add("330000");
        codes.add("340000");
        codes.add("350000");
        codes.add("360000");
        codes.add("370000");
        codes.add("410000");
        codes.add("420000");
        codes.add("430000");
        codes.add("440000");
        codes.add("450000");
        codes.add("460000");
        codes.add("510000");
        codes.add("520000");
        codes.add("530000");
        codes.add("540000");
        codes.add("610000");
        codes.add("620000");
        codes.add("630000");
        codes.add("640000");
        codes.add("650000");
        codes.add("710000");
        return codes;
    }

    private static void updateChildren(String code) {
        DistrictResource districtResource = new DistrictResource();
        ListResponse<DistrictSimple> data = districtResource.children(code);

        List<AreaRequest> requests=new ArrayList<>();
        for (DistrictSimple datum : data.getData()) {

            AreaRequest temp=new AreaRequest();
            temp.setName(datum.getName());
            temp.setCode(datum.getId());
            temp.setFullName(datum.getFullname());
            temp.setLat(datum.getLocation().getLat());
            temp.setLng(datum.getLocation().getLng());
            requests.add(temp);
        }

        Map<String, Object> datum =new HashMap<>();
        datum.put("items",requests);
        datum.put("code", code);

        HttpRequest request = HttpRequest.post("http://39.103.128.165:6200/action/area/syncChild");
        request.contentType("application/json");
        request.charset("utf-8");
        //参数详情
        String body = new Gson().toJson(datum);
        request.body(body);
        request.bodyText(body, "application/json", "utf-8");

        HttpResponse response = request.send();
        String respJson = response.bodyText();
        System.out.println(respJson);
    }
}
