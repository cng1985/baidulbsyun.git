package com.nbsaas.lbs.qq.exams;

import com.nbsaas.http.Connection;
import com.nbsaas.http.HttpConnection;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SignApp {

    public static void main(String[] args) throws IOException {

        String sign=getMD5Hash("/ws/district/v1/list?key=3GWBZ-7THRC-DZI2F-ABUQ7-CILMV-ACFB7kK27ubm3sYd79rofB9cs9PMHFqxcOdRy");

        Connection connection = HttpConnection.connect("https://apis.map.qq.com/ws/district/v1/list");
        connection.data("key", "3GWBZ-7THRC-DZI2F-ABUQ7-CILMV-ACFB7");
        connection.data("sig", sign);

        connection.method(Connection.Method.GET);
        String body = connection.execute().body();
        System.out.println(body);

        System.out.println(sign);
        System.out.println(connection);

    }

    public static String getMD5Hash(String input) {
        try {
            // 获取MD5实例
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 将字符串转换为字节数组并更新摘要
            md.update(input.getBytes());
            // 计算摘要（哈希值）
            byte[] digest = md.digest();
            // 将字节数组转换为16进制格式的字符串
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5 algorithm not found", e);
        }
    }

}
