package com.nbsaas.lbs.qq.placeCloud.api;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.nbsaas.lbs.qq.placeCloud.domain.BackResponse;
import com.nbsaas.lbs.qq.placeCloud.domain.DataCreateRequest;
import com.nbsaas.lbs.qq.placeCloud.domain.DataDeleteRequest;
import com.nbsaas.lbs.qq.placeCloud.domain.DataUpdateRequest;
import com.nbsaas.lbs.qq.v1.domain.response.Poi;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DataService implements DataApi {
    @Override
    public BackResponse create(DataCreateRequest createRequest){
        BackResponse result = new BackResponse();

        HttpRequest request = HttpRequest.post("https://apis.map.qq.com/place_cloud/data/create");
        request.contentType("application/json");
        request.charset("utf-8");
        //参数详情
        if (createRequest != null) {
            String body=getGson().toJson(createRequest);
            System.out.println(body);
            request.body(body);
            request.bodyText(body, "application/json", "utf-8");

        }
                HttpResponse response = request.send();
        String respJson = response.bodyText();
        System.out.println(respJson);

        return result;
    }

    @NotNull
    private BackResponse okhttp(DataCreateRequest createRequest) {
        BackResponse result = new BackResponse();


        String body1=getGson().toJson(createRequest);
//            System.out.println(body);
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, body1);
        Request request = new Request.Builder()
                .url("https://apis.map.qq.com/place_cloud/data/create")
                .post(body)
                .build();

//        HttpResponse response = request.send();
        String respJson = null;
        try {
            Response response = client.newCall(request).execute();
            respJson = response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println(respJson);
        return result;
    }

    @Override
    public BackResponse update(DataUpdateRequest createRequest) {
        BackResponse result = new BackResponse();
        HttpRequest request = HttpRequest.post("https://apis.map.qq.com/place_cloud/data/update");
        request.contentType("application/json");
        request.charset("utf-8");

        //参数详情
        if (createRequest != null) {
            createRequest.setFilter("ud_id in (\""+createRequest.getItems().get(0).getId()+"\")");
            String body=new Gson().toJson(createRequest);
            System.out.println(body);
            request.body(body);
        }

        HttpResponse response = request.send();
        String respJson = response.bodyText();
        System.out.println(respJson);
        return result;
    }

    @Override
    public BackResponse delete(DataDeleteRequest createRequest) {
        BackResponse result = new BackResponse();
        HttpRequest request = HttpRequest.post("https://apis.map.qq.com/place_cloud/data/delete");
        request.contentType("application/json");
        request.charset("utf-8");

        //参数详情
        if (createRequest != null) {
            createRequest.setFilter("ud_id  in ( \""+createRequest.getId()+"\" )");
            Gson gs = new GsonBuilder()
                    .setPrettyPrinting()
                    .disableHtmlEscaping()
                    .create();
            String body=gs.toJson(createRequest);
            System.out.println(body);
            request.body(body);
        }

        HttpResponse response = request.send();
        String respJson = response.bodyText();
        System.out.println(respJson);
        return result;
    }

    @Override
    public List<Poi> list(DataDeleteRequest createRequest) {
        List<Poi> result = new ArrayList<>();
        HttpRequest request = HttpRequest.get("https://apis.map.qq.com/place_cloud/data/list");
        request.charset("utf-8");
        request.query("key",createRequest.getKey());
        request.query("table_id",createRequest.getTableId());
        //参数详情
        HttpResponse response = request.send();
        String respJson = response.bodyText();
        System.out.println(respJson);
        JsonParser parser=new JsonParser();
        JsonElement root= parser.parse(respJson);
        JsonElement datas= root.getAsJsonObject().get("result").getAsJsonObject().get("data");
        if (datas!=null){
            System.out.println(datas);
            result=getGson().fromJson(datas.toString(),new TypeToken<List<Poi>>(){}.getType());
        }
        return result;
    }

    public Gson getGson(){
        Gson gs = new GsonBuilder()
                .setPrettyPrinting()
                .disableHtmlEscaping()
                .create();
        return gs;
    }
}
