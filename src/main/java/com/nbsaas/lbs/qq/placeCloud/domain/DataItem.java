package com.nbsaas.lbs.qq.placeCloud.domain;

import com.google.gson.annotations.SerializedName;
import com.nbsaas.lbs.qq.v1.domain.response.Location;
import lombok.Data;

import java.io.Serializable;

@Data
public class DataItem implements Serializable {

    @SerializedName("ud_id")
    private String id;

    private String title;

    private String tel;


    private Location location;
}
