package com.nbsaas.lbs.qq.placeCloud.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class BackResponse implements Serializable {

    private int status;

    private String message;


}
