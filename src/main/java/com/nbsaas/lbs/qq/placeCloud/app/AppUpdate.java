package com.nbsaas.lbs.qq.placeCloud.app;

import com.nbsaas.lbs.qq.placeCloud.api.DataService;
import com.nbsaas.lbs.qq.placeCloud.domain.BackResponse;
import com.nbsaas.lbs.qq.placeCloud.domain.DataDeleteRequest;
import com.nbsaas.lbs.qq.placeCloud.domain.DataItem;
import com.nbsaas.lbs.qq.placeCloud.domain.DataUpdateRequest;
import com.nbsaas.lbs.qq.v1.builder.ServicesBuilder;
import com.nbsaas.lbs.qq.v1.domain.request.SearchByNearbyRequest;
import com.nbsaas.lbs.qq.v1.domain.response.Poi;
import com.nbsaas.lbs.qq.v1.domain.response.Pois;
import com.nbsaas.lbs.qq.v1.service.PlaceService;

import java.util.List;

public class AppUpdate {

    public static void main(String[] args) {

        DataService dataService = new DataService();
        DataUpdateRequest request=new DataUpdateRequest();
        DataItem item=new DataItem();
        item.setId("18");
        item.setTitle("测试");
        request.getItems().add(item);


        request.setKey("LUCBZ-NE5KK-7UEJH-AFXWU-3Q6SO-PPFQI");
        request.setTableId("60752ee1d14276473106060d");
        BackResponse res = dataService.update(request);
        System.out.println(res);
    }
}
