package com.nbsaas.lbs.baidu.webservice.domain;

import java.io.Serializable;
import java.util.List;

/**
 * 地点集合
 * 
 * @author 年高
 *
 */
public class BackSuggestion implements Serializable {
	@Override
	public String toString() {
		return "BackSuggestion [status=" + status + ", message=" + message
				+ ", result=" + result + "]";
	}

	private int status;
	private String message;
	private List<BackSuggestionInfo> result;

	public List<BackSuggestionInfo> getResult() {
		return result;
	}

	public void setResult(List<BackSuggestionInfo> result) {
		this.result = result;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
