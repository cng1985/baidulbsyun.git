package com.nbsaas.lbs.baidu.webservice.domain;

import java.io.Serializable;

public class BackPoi implements Serializable {

	/**
	 * poi名称
	 */
	private String name;
	/**
	 * poi地址信息
	 */
	private String address;
	private String street_id;
	/**
	 * poi的唯一标示
	 */
	private String uid;
	/**
	 * poi电话信息
	 */
	private String telephone;
	/**
	 * poi经纬度坐标
	 */
	private BackLocation location;
	private BackDetailInfo detail_info;

	public String getAddress() {
		return address;
	}

	public BackDetailInfo getDetail_info() {
		return detail_info;
	}

	public BackLocation getLocation() {
		return location;
	}

	public String getName() {
		return name;
	}

	public String getStreet_id() {
		return street_id;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getUid() {
		return uid;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setDetail_info(BackDetailInfo detail_info) {
		this.detail_info = detail_info;
	}

	public void setLocation(BackLocation location) {
		this.location = location;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStreet_id(String street_id) {
		this.street_id = street_id;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	@Override
	public String toString() {
		return "BackPoi [name=" + name + ", address=" + address + ", location="
				+ location + ", street_id=" + street_id + ", uid=" + uid
				+ ", telephone=" + telephone + ", detail_info=" + detail_info
				+ "]";
	}

}
