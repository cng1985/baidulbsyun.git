package com.nbsaas.lbs.baidu.webservice.v2;

import java.io.IOException;

import com.google.gson.Gson;
import com.nbsaas.http.Connection;
import com.nbsaas.http.HttpConnection;
import com.nbsaas.http.Connection.Method;
import com.nbsaas.lbs.baidu.webservice.domain.BackSuggestion;

public class SuggestionService {

	private String suggestion = "http://api.map.baidu.com/place/v2/suggestion";
	private String region;

	public static void main(String[] args) {
		SuggestionService s=new SuggestionService();
		//s.region="西安";
		BackSuggestion ss=	s.searchByCircle("汽车");
		System.out.println(ss);
	}

	/**
	 * @see BackSuggestion
	 * @param keyword 关键词
	 * @return 地点集合
	 */
	public BackSuggestion searchByCircle(String keyword) {
		BackSuggestion reult = null;
		Connection connection = HttpConnection.connect(suggestion);
		connection.data("q", keyword);
		// json或xml
		connection.data("output", "json");
		// connection.data("tag", keyword); 日式烧烤/铁板烧、朝外大街
		// 检索结果详细程度。取值为1 或空，则返回基本信息；取值为2，返回检索POI详细信息
		connection.data("region", "" + region);

		connection.data("ak", ServiceConfig.ak);
		connection.method(Method.GET);
		try {
			String back = connection.execute().body();
			System.out.println(back);
			Gson gson = new Gson();
			reult = gson.fromJson(back, BackSuggestion.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reult;

	}
}
