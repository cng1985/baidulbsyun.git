package com.nbsaas.lbs.baidu.geo;

import java.io.Serializable;

public class LbsAddress implements Serializable {

	private String nation;
	private String province;
	private String city;
	private String district;
	private String street;
	private String street_number;
	private String formatted_address;
	public String getFormatted_address() {
		return formatted_address;
	}

	public void setFormatted_address(String formatted_address) {
		this.formatted_address = formatted_address;
	}

	public String getStreet_number() {
		return street_number;
	}

	public void setStreet_number(String street_number) {
		this.street_number = street_number;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	@Override
	public String toString() {
		return "LbsAddress [nation=" + nation + ", province=" + province
				+ ", city=" + city + ", district=" + district + ", street="
				+ street + ", street_number=" + street_number + "]";
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public boolean isok() {
		boolean result = false;
		if (province != null && province.length() > 0) {
			result = true;
		}
		return result;
	}

}
