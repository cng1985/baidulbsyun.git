package com.nbsaas.lbs.baidu.geo;

import java.io.IOException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nbsaas.http.Connection;
import com.nbsaas.http.HttpConnection;
import com.nbsaas.http.Connection.Method;

/**
 * 百度地理位置转化
 * 
 * @author 年高
 *
 */
public class BaiduGeoConv {
	private static String nearbyurl = "http://api.map.baidu.com/geoconv/v1/?";
	private static String ak = "19fe04c380d6f64884191e21fdca9043";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// 34.2395504,108.9011901 34.2395196,108.9011494 34.2395246,108.9011531
		String value = "34.27779990,108.95309828";
		String lbs = get(value,"5","5");
		System.out.println(lbs);

	}

	/**
	 * 1：GPS设备获取的角度坐标;<br/>
	 * 
	 * 2：GPS获取的米制坐标、sogou地图所用坐标;<br/>
	 * 
	 * 3：google地图、soso地图、aliyun地图、mapabc地图和amap地图所用坐标<br/>
	 * 
	 * 4：3中列表地图坐标对应的米制坐标<br/>
	 * 
	 * 5：百度地图采用的经纬度坐标<br/>
	 * 
	 * 6：百度地图采用的米制坐标<br/>
	 * 
	 * 7：mapbar地图坐标;<br/>
	 * 
	 * 8：51地图坐标<br/>
	 * */
	static String get(String value, String from, String to) {
		String lbs = "";
		Connection connection = HttpConnection.connect(nearbyurl);
		connection.data("ak", ak);
		connection.data("coords", value);
		connection.data("to",from);

		connection.data("from",to);

		connection.data("output", "json");
		connection.method(Method.GET);
		String msg = null;
		try {
			msg = connection.execute().body();
			System.out.println(msg);
			JsonParser jsonParser = new JsonParser();
			JsonObject resp = jsonParser.parse(msg).getAsJsonObject();
			JsonElement result = resp.get("result");
			JsonArray a=	result.getAsJsonArray();
			resp=a.get(0).getAsJsonObject();
			lbs=getkey(resp, "x")+","+getkey(resp, "y");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lbs;
	}

	private static String getkey(JsonObject address_component, String key) {
		String result = "";
		try {
			result = address_component.getAsJsonObject().get(key).getAsString();
		} catch (Exception e) {

		}
		return result;
	}
}
