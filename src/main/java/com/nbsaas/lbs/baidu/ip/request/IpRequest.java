package com.nbsaas.lbs.baidu.ip.request;

public class IpRequest {
  
  private String ip;
  
  public String getIp() {
    return ip;
  }
  
  public void setIp(String ip) {
    this.ip = ip;
  }
}
