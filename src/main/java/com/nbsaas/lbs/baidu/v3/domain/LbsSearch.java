package com.nbsaas.lbs.baidu.v3.domain;

import java.util.List;


public class LbsSearch {

	private String message;
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private int status;
	private int size;
	private int total;

	private List<SearchPoi> contents;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<SearchPoi> getContents() {
		return contents;
	}

	public void setContents(List<SearchPoi> contents) {
		this.contents = contents;
	}

	@Override
	public String toString() {
		return "LbsSearch [message=" + message + ", status=" + status
				+ ", size=" + size + ", total=" + total + ", contents="
				+ contents + "]";
	}
	
}
