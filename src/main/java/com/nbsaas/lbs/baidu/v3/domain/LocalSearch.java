package com.nbsaas.lbs.baidu.v3.domain;

import java.io.Serializable;

public class LocalSearch implements Serializable {

	//检索关键字
	private String q;
	//坐标系 3代表百度经纬度坐标系统
	private int coord_type=3;
	//检索区域名称 市或区的名字，如北京市，海淀区。可选,此接口推荐填写该参数 否则，默认按照全国范围来检索
	private String region;
	//标签 空格分隔的多字符串  样例：美食 小吃
	private String tags;
	//排序字段 “|”分隔的多个检索条件。
	private String sortby;
	//过滤条件样例：筛选价格为9.99到19.99并且生产时间为2013年的项rice:9.99,19.99|time:2012,2012
	private String filter;
	//分页索引	uint32	当前页标，从0开始   可选   默认为0
	private int page_index;
	//分页数量	uint32	当前页面最大结果数	可选  默认为10，最多为50
	
	private int page_size;
	//回调函数
	private String callback;
	//用户的权限签名
	private String sn;
	public String getQ() {
		return q;
	}
	public void setQ(String q) {
		this.q = q;
	}
	public int getCoord_type() {
		return coord_type;
	}
	public void setCoord_type(int coord_type) {
		this.coord_type = coord_type;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getSortby() {
		return sortby;
	}
	public void setSortby(String sortby) {
		this.sortby = sortby;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public int getPage_index() {
		return page_index;
	}
	public void setPage_index(int page_index) {
		this.page_index = page_index;
	}
	public int getPage_size() {
		return page_size;
	}
	public void setPage_size(int page_size) {
		this.page_size = page_size;
	}
	public String getCallback() {
		return callback;
	}
	public void setCallback(String callback) {
		this.callback = callback;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	

}
