package com.nbsaas.lbs.baidu.v3.service;

import com.nbsaas.http.Connection;
import com.nbsaas.http.Connection.Method;
import com.nbsaas.lbs.baidu.v3.domain.State;
import com.nbsaas.lbs.baidu.v3.api.PoiService;
import com.nbsaas.lbs.baidu.v3.domain.Poi;
import com.nbsaas.lbs.baidu.v3.domain.PoiDetailState;
import com.nbsaas.lbs.baidu.v3.request.PoiDeleteRequest;
import com.nbsaas.lbs.baidu.v3.request.PoiListRequest;
import com.nbsaas.lbs.baidu.v3.request.PoiUpdateRequest;
import com.nbsaas.lbs.baidu.v3.response.PoiDeleteResponse;
import com.nbsaas.lbs.baidu.v3.response.PoiListResponse;
import com.nbsaas.lbs.baidu.v3.response.PoiUpdateResponse;
import com.nbsaas.utils.StringUtils;

import java.util.Map;

public class PoiServiceImpl extends BaseService implements PoiService {
  
  String createurl = "http://api.map.baidu.com/geodata/v3/poi/create";
  String detailurl = "http://api.map.baidu.com/geodata/v3/poi/detail";
  String listurl = "http://api.map.baidu.com/geodata/v3/poi/list";
  String deleteurl = "http://api.map.baidu.com/geodata/v3/poi/delete";
  String updateurl = "http://api.map.baidu.com/geodata/v3/poi/update";
  
  
  public PoiServiceImpl(Config config) {
    super(config);
  }
  
  @Override
  public State add(Poi poi) {
    State result = null;
    Connection connection = getConnection(createurl);
    connection.data("address", poi.getAddress()).data("title",
        poi.getTitle());
    connection.data("latitude", "" + poi.getLatitude());
    connection.data("longitude", "" + poi.getLongitude());
    connection.data("coord_type", "" + poi.getCoord_type());
    buildParams(connection, poi.getData());
    connection.method(Method.POST);
    return execute(connection,State.class);
  }
  

  

  
  @Override
  public PoiDetailState findById(int id) {
    PoiDetailState result = null;
    Connection connection = getConnection(detailurl);
    connection.data("id", "" + id);
    connection.method(Method.GET);
    return execute(connection,PoiDetailState.class);
  
  }
  
  @Override
  public PoiListResponse list(PoiListRequest request) {
    PoiListResponse result = null;
    Connection connection = getConnection(listurl);
    if (StringUtils.isNotBlank(request.getTitle())) {
      connection.data("title", "" + request.getTitle());
    }
    if (StringUtils.isNotBlank(request.getTags())) {
      connection.data("tags", "" + request.getTags());
    }
    if (StringUtils.isNotBlank(request.getBounds())) {
      connection.data("bounds", "" + request.getBounds());
    }
    Map<String, String> maps = request.getParams();
    buildParams(connection, maps);
    connection.data("coord_type", "" + request.getCoordType());
    connection.data("page_index", "" + request.getNo());
    connection.data("page_size", "" + request.getSize());
    
    connection.method(Method.GET);
    return execute(connection,PoiListResponse.class);
  
  }
  
  
  @Override
  public PoiDeleteResponse delete(PoiDeleteRequest request) {
    PoiDeleteResponse result = null;
    Connection connection = getConnection(deleteurl);
    if (StringUtils.isNotBlank(request.getTitle())) {
      connection.data("title", "" + request.getTitle());
    }
    if (StringUtils.isNotBlank(request.getTags())) {
      connection.data("tags", "" + request.getTags());
    }
    if (StringUtils.isNotBlank(request.getBounds())) {
      connection.data("bounds", "" + request.getBounds());
    }
    connection.data("is_total_del", "1");
    buildParams(connection, request.getParams());
    connection.method(Method.POST);
    return execute(connection,PoiDeleteResponse.class);
  }
  
  @Override
  public PoiUpdateResponse update(PoiUpdateRequest request) {
    PoiUpdateResponse result = null;
    Connection connection = getConnection(updateurl);
    connection.data("id", request.getId() + "");
    if (StringUtils.isNotBlank(request.getTitle())) {
      connection.data("title", "" + request.getTitle());
    }
    if (StringUtils.isNotBlank(request.getTags())) {
      connection.data("tags", "" + request.getTags());
    }
    if (request.getLatitude() > 0) {
      connection.data("latitude", "" + request.getLatitude());
    }
    if (request.getLongitude() > 0) {
      connection.data("longitude", "" + request.getLongitude());
    }
    
    buildParams(connection,  request.getParams());
    connection.method(Method.POST);
    return execute(connection,PoiUpdateResponse.class);

  }
  

}
