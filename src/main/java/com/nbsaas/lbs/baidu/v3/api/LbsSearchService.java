package com.nbsaas.lbs.baidu.v3.api;

import com.nbsaas.lbs.baidu.v3.domain.BoundsSearch;
import com.nbsaas.lbs.baidu.v3.domain.LocalSearch;
import com.nbsaas.lbs.baidu.v3.domain.Search;
import com.nbsaas.lbs.baidu.v3.domain.LbsSearch;

public interface LbsSearchService {
  
  LbsSearch serachForNearby(Search search);
  
  LbsSearch findBylocal(LocalSearch search);
  
  LbsSearch findBybound(BoundsSearch search);
}
