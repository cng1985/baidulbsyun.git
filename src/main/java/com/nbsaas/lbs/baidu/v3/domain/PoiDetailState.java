package com.nbsaas.lbs.baidu.v3.domain;

import java.io.Serializable;


public class PoiDetailState implements Serializable {

	private String message;
	private int status;
	private Poi poi;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Poi getPoi() {
		return poi;
	}
	public void setPoi(Poi poi) {
		this.poi = poi;
	}
	@Override
	public String toString() {
		return "PoiDetailState [message=" + message + ", status=" + status
				+ ", poi=" + poi + "]";
	}
}
