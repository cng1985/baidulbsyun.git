package com.nbsaas.lbs.baidu.v3.api;

import com.nbsaas.lbs.baidu.v3.domain.PoiDetailState;
import com.nbsaas.lbs.baidu.v3.domain.State;
import com.nbsaas.lbs.baidu.v3.request.PoiDeleteRequest;
import com.nbsaas.lbs.baidu.v3.request.PoiListRequest;
import com.nbsaas.lbs.baidu.v3.response.PoiDeleteResponse;
import com.nbsaas.lbs.baidu.v3.response.PoiUpdateResponse;
import com.nbsaas.lbs.baidu.v3.domain.Poi;
import com.nbsaas.lbs.baidu.v3.request.PoiUpdateRequest;
import com.nbsaas.lbs.baidu.v3.response.PoiListResponse;

public interface PoiService {
  
  /**
   * 添加poi信息
   * @param poi
   * @return
   */
  State add(Poi poi);
  
  PoiDetailState findById(int id);
  
  PoiListResponse list(PoiListRequest request);
  
  PoiDeleteResponse delete(PoiDeleteRequest request);
  
  PoiUpdateResponse update(PoiUpdateRequest request);
}
