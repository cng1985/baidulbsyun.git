package com.nbsaas.lbs.baidu.v3.request;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class PoiUpdateRequest implements Serializable {
  
  private Long id;
  
  private String title;
  private String address;
  private String tags;
  private double latitude;
  private double longitude;
  private int coordType=3;
  private Map<String,String> params=new HashMap<String, String>();
  
  public String getTitle() {
    return title;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }
  
  public String getAddress() {
    return address;
  }
  
  public void setAddress(String address) {
    this.address = address;
  }
  
  public String getTags() {
    return tags;
  }
  
  public void setTags(String tags) {
    this.tags = tags;
  }
  
  public double getLatitude() {
    return latitude;
  }
  
  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }
  
  public double getLongitude() {
    return longitude;
  }
  
  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }
  
  public int getCoordType() {
    return coordType;
  }
  
  public void setCoordType(int coordType) {
    this.coordType = coordType;
  }
  
  public Map<String, String> getParams() {
    return params;
  }
  
  public void setParams(Map<String, String> params) {
    this.params = params;
  }
  
  public Long getId() {
    return id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
}
