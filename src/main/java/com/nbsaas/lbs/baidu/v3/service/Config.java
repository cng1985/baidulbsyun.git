package com.nbsaas.lbs.baidu.v3.service;

public class Config {
	private   String ak ;
	
	private  String geotable;
	
	public String getAk() {
		return ak;
	}
	
	public void setAk(String ak) {
		this.ak = ak;
	}
	
	public String getGeotable() {
		return geotable;
	}
	
	public void setGeotable(String geotable) {
		this.geotable = geotable;
	}
}
