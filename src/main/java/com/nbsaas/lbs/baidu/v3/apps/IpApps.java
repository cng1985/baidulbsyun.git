package com.nbsaas.lbs.baidu.v3.apps;

import com.nbsaas.lbs.baidu.builder.ServiceEngine;
import com.nbsaas.lbs.baidu.builder.ServiceEngineBuilder;
import com.nbsaas.lbs.baidu.ip.request.IpRequest;
import com.nbsaas.lbs.baidu.ip.response.IpResponse;
import com.nbsaas.lbs.baidu.ip.service.IpService;

public class IpApps {
  public static void main(String[] args) {
    ServiceEngineBuilder builder = ServiceEngineBuilder.newBuilder();
    builder.ak("5UMODI65jVANU8FVkKTt3CfCCUMTA1u0");
    builder.geotable("191439");
    ServiceEngine serviceEngine = builder.build();
    IpService service = serviceEngine.getIpService();
    IpRequest request = new IpRequest();
    //request.setIp("1.80.219.103");
    IpResponse response = service.ip(request);
    System.out.println(response);
  }
}
