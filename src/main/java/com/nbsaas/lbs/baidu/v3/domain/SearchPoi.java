package com.nbsaas.lbs.baidu.v3.domain;

import java.io.Serializable;
import java.util.List;

public class SearchPoi implements Serializable {
	private String uid;
	private String geotable_id;

	private String title;
	private String address;
	private String tags;
	private String province;
	private String city;
	private String district;
	private int coord_type;
	private List<Double> location;
	private int distance;
	private int weight;
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getGeotable_id() {
		return geotable_id;
	}
	public void setGeotable_id(String geotable_id) {
		this.geotable_id = geotable_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public int getCoord_type() {
		return coord_type;
	}
	public void setCoord_type(int coord_type) {
		this.coord_type = coord_type;
	}
	public List<Double> getLocation() {
		return location;
	}
	public void setLocation(List<Double> location) {
		this.location = location;
	}
	public int getDistance() {
		return distance;
	}
	public void setDistance(int distance) {
		this.distance = distance;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	@Override
	public String toString() {
		return "SearchPoi [uid=" + uid + ", geotable_id=" + geotable_id
				+ ", title=" + title + ", address=" + address + ", tags="
				+ tags + ", province=" + province + ", city=" + city
				+ ", district=" + district + ", coord_type=" + coord_type
				+ ", location=" + location + ", distance=" + distance
				+ ", weight=" + weight + "]";
	}

}
