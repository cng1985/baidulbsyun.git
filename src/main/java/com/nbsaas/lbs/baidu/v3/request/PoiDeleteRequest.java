package com.nbsaas.lbs.baidu.v3.request;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class PoiDeleteRequest implements Serializable {
  
  /**
   *  位置数据名称
   */
  private String title;
  
  /**
   * 位置数据类别
   */
  private String tags;
  
  /**
   * 查询的矩形区域,格式x1,y1;x2,y2分别代表矩形的左上角和右下角。
   范围过大时服务易超时，返回数据为空。
   建议经度跨度小于0.8，纬度跨度小于0.5。
   */
  private String bounds;
  

  private Long id;
  
  private String ids;
  
  private Map<String,String> params=new HashMap<String, String>();
  
  public String getTitle() {
    return title;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }
  
  public String getTags() {
    return tags;
  }
  
  public void setTags(String tags) {
    this.tags = tags;
  }
  
  public String getBounds() {
    return bounds;
  }
  
  public void setBounds(String bounds) {
    this.bounds = bounds;
  }
  
  public Long getId() {
    return id;
  }
  
  public void setId(Long id) {
    this.id = id;
  }
  
  public String getIds() {
    return ids;
  }
  
  public void setIds(String ids) {
    this.ids = ids;
  }
  
  public Map<String, String> getParams() {
    return params;
  }
  
  public void setParams(Map<String, String> params) {
    this.params = params;
  }
}
