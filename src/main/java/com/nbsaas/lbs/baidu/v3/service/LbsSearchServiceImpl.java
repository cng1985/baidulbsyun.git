package com.nbsaas.lbs.baidu.v3.service;

import com.nbsaas.lbs.baidu.v3.api.LbsSearchService;
import com.nbsaas.lbs.baidu.v3.domain.BoundsSearch;
import com.nbsaas.lbs.baidu.v3.domain.LbsSearch;
import com.nbsaas.lbs.baidu.v3.domain.LocalSearch;
import com.nbsaas.lbs.baidu.v3.domain.Search;
import com.nbsaas.utils.StringUtils;

import com.nbsaas.http.Connection;
import com.nbsaas.http.Connection.Method;

public class LbsSearchServiceImpl extends BaseService implements LbsSearchService {

	private String nearbyurl = "http://api.map.baidu.com/geosearch/v3/nearby";
	private String localurl = "http://api.map.baidu.com/geosearch/v3/local";
	private String boundurl = "http://api.map.baidu.com/geosearch/v3/bound";
	
	private Config config;
	
	public LbsSearchServiceImpl(Config config) {
		super(config);
	}
	
	
	@Override
	public LbsSearch serachForNearby(Search search) {
		LbsSearch result = null;
		Connection connection =	getConnection(nearbyurl);
	
		if (StringUtils.isNotBlank( search.getQ())){
			connection.data("q", "" + search.getQ());
		}
		connection.data("location", "" + search.getLocation());
		connection.data("coord_type", "" + search.getCoord_type());
		connection.data("radius", "" + search.getRadius());
		if (search.getTags() != null) {
			connection.data("tags", "" + search.getTags());

		}
		if (search.getSortby() != null) {
			connection.data("sortby", "" + search.getSortby());

		}
		if (search.getFilter() != null) {
			connection.data("filter", "" + search.getFilter());

		}
		connection.data("page_index", "" + search.getPage_index());
		connection.data("page_size", "" + search.getPage_size());
		if (search.getCallback() != null) {
			connection.data("callback", "" + search.getCallback());

		}
		if (search.getSn() != null) {
			connection.data("sn", "" + search.getSn());
		}
		connection.method(Method.GET);
		return execute(connection,LbsSearch.class);
		
	}

	@Override
	public LbsSearch findBylocal(LocalSearch search) {
		LbsSearch result = null;
		Connection connection =	getConnection(nearbyurl);
		connection.data("q", "" + search.getQ());
		connection.data("coord_type", "" + search.getCoord_type());
		if (search.getRegion() != null) {
			connection.data("region", "" + search.getRegion());
		}
		if (search.getTags() != null) {
			connection.data("tags", "" + search.getTags());

		}
		if (search.getSortby() != null) {
			connection.data("sortby", "" + search.getSortby());

		}
		if (search.getFilter() != null) {
			connection.data("filter", "" + search.getFilter());

		}
		connection.data("page_index", "" + search.getPage_index());
		connection.data("page_size", "" + search.getPage_size());
		if (search.getCallback() != null) {
			connection.data("callback", "" + search.getCallback());

		}
		if (search.getSn() != null) {
			connection.data("sn", "" + search.getSn());
		}
		return execute(connection,LbsSearch.class);
		
	}

	@Override
	public LbsSearch findBybound(BoundsSearch search) {
		LbsSearch result = null;
		Connection connection =	getConnection(nearbyurl);
		connection.data("q", "" + search.getQ());
		connection.data("coord_type", "" + search.getCoord_type());
		connection.data("bounds", "" + search.getBounds());
		if (search.getTags() != null) {
			connection.data("tags", "" + search.getTags());

		}
		if (search.getSortby() != null) {
			connection.data("sortby", "" + search.getSortby());

		}
		if (search.getFilter() != null) {
			connection.data("filter", "" + search.getFilter());

		}
		connection.data("page_index", "" + search.getPage_index());
		connection.data("page_size", "" + search.getPage_size());
		if (search.getCallback() != null) {
			connection.data("callback", "" + search.getCallback());

		}
		if (search.getSn() != null) {
			connection.data("sn", "" + search.getSn());
		}
		return execute(connection,LbsSearch.class);
		
	}
}
