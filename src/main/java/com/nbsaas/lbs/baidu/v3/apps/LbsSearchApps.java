package com.nbsaas.lbs.baidu.v3.apps;

import com.nbsaas.lbs.baidu.builder.ServiceEngine;
import com.nbsaas.lbs.baidu.builder.ServiceEngineBuilder;
import com.nbsaas.lbs.baidu.v3.api.LbsSearchService;
import com.nbsaas.lbs.baidu.v3.domain.BoundsSearch;
import com.nbsaas.lbs.baidu.v3.domain.LocalSearch;
import com.nbsaas.lbs.baidu.v3.domain.Search;
import com.nbsaas.lbs.baidu.v3.domain.LbsSearch;

public class LbsSearchApps {
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    aa();
  }
  
  private static void ss() {
    ServiceEngineBuilder builder = ServiceEngineBuilder.newBuilder();
    builder.ak("5UMODI65jVANU8FVkKTt3CfCCUMTA1u0");
    builder.geotable("191439");
    ServiceEngine serviceEngine = builder.build();
    LbsSearchService service = serviceEngine.getV3LbsSearchService();
    BoundsSearch search = new BoundsSearch();
    search.setQ("");
    search.setBounds("107.950524,34.269067;117.30,37.20");
    search.setPage_size(50);
    LbsSearch s = service.findBybound(search);
    System.out.println(s);
  }
  
  private static void loc(LbsSearchService service) {
    LocalSearch search = new LocalSearch();
    search.setQ("");
    search.setRegion("西安");
    search.setPage_size(50);
    LbsSearch s = service.findBylocal(search);
    System.out.println(s);
  }
  
  private static void aa() {
    ServiceEngineBuilder builder = ServiceEngineBuilder.newBuilder();
    builder.ak("5UMODI65jVANU8FVkKTt3CfCCUMTA1u0");
    builder.geotable("191439");
    ServiceEngine serviceEngine = builder.build();
    LbsSearchService service = serviceEngine.getV3LbsSearchService();
    Search search = new Search();
    search.setQ("");
    search.setLocation("108.95272064209,34.270034790039");
    search.setPage_size(50);
    LbsSearch s = service.serachForNearby(search);
    System.out.println(s);
  }
  
}
