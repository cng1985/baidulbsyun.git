package com.nbsaas.lbs.amap.v3.service;

import com.nbsaas.lbs.amap.domain.AreaInfo;
import com.nbsaas.lbs.amap.v3.domain.request.SearchRequest;

import java.util.List;

/**
 * 高德地图行政区域接口
 */
public interface DistrictService {
  
  /**
   * 查询某个地区下面的地区
   *
   * @param code
   * @return
   */
  List<AreaInfo> area(String code);
  
  /**
   * 中国所有的省份
   *
   * @return
   */
  List<AreaInfo> provinces();
  
  /**
   * 地区搜索
   *
   * @param request
   * @return
   */
  List<AreaInfo> search(SearchRequest request);
}
