package com.nbsaas.lbs.amap.v3.app;

import com.nbsaas.lbs.amap.domain.AreaInfo;
import com.nbsaas.lbs.amap.v3.domain.request.SearchRequest;
import com.nbsaas.lbs.amap.v3.service.impl.DistrictServiceImpl;
import com.nbsaas.lbs.baidu.v3.service.Config;

import java.util.List;

public class DistrictApp {
  public static void main(String[] args) {
    Config config = new Config();
    config.setAk("c3ed521babcc6c3b1af2467064d216d7");
    DistrictServiceImpl app = new DistrictServiceImpl(config);
    
    SearchRequest request = new SearchRequest();
    request.setKeywords("重庆市");
    request.setSubdistrict("3");
    List<AreaInfo> areaInfos = app.search(request);
    print(areaInfos);
  }
  
  private static void print(List<AreaInfo> areaInfos) {
    System.out.println("*************childs****************");
    for (AreaInfo areaInfo : areaInfos) {
      System.out.println(areaInfo);
      if (areaInfo.getChilds().size()>0){
        print(areaInfo.getChilds());
      }
    }
  }
}
